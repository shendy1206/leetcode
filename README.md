# leetcode

### Discuss
* [Cells with Odd Values in a Matrix](https://leetcode.com/problems/cells-with-odd-values-in-a-matrix/)
* [Jewels and Stones](https://leetcode.com/problems/jewels-and-stones/)
* [Range Sum of BST](https://leetcode.com/problems/range-sum-of-bst/)
* [Split a String in Balanced Strings](https://leetcode.com/problems/split-a-string-in-balanced-strings/)
* [To Lower Case](https://leetcode.com/problems/to-lower-case/)
* [Remove Outermost Parentheses](https://leetcode.com/problems/remove-outermost-parentheses/)
* [Unique Morse Code Words](https://leetcode.com/problems/unique-morse-code-words/)
* [Fibonacci Number](https://leetcode.com/problems/fibonacci-number/)
 
### Amazon Interview Problems List
* [Split Linked List in Parts](https://leetcode.com/problems/split-linked-list-in-parts/discuss/491631/faster-than-100.00)
* [Top K Frequent Words](https://leetcode.com/problems/top-k-frequent-words/discuss/493365/Use-stream)

### Fibonacci
* [Fibonacci Number](https://leetcode.com/problems/fibonacci-number/)
* [Split Array into Fibonacci Sequence (Backtracking)](https://leetcode.com/problems/split-array-into-fibonacci-sequence/)
* [Length of Longest Fibonacci Subsequence](https://leetcode.com/problems/length-of-longest-fibonacci-subsequence/)



