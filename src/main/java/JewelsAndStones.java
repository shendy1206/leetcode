/**
 * 771. Jewels and Stones
 * Easy
 *
 * 2198
 *
 * 376
 *
 * Add to List
 *
 * Share
 * You're given strings J representing the types of stones that are jewels, and S representing the stones you have.  Each character in S is a type of stone you have.  You want to know how many of the stones you have are also jewels.
 *
 * The letters in J are guaranteed distinct, and all characters in J and S are letters. Letters are case sensitive, so "a" is considered a different type of stone from "A".
 *
 * Example 1:
 *
 * Input: J = "aA", S = "aAAbbbb"
 * Output: 3
 * Example 2:
 *
 * Input: J = "z", S = "ZZ"
 * Output: 0
 * Note:
 *
 * S and J will consist of letters and have length at most 50.
 * The characters in J are distinct.
 */

public class JewelsAndStones {
	public int numJewelsInStones(String J, String S) {


		int[] chars = new int['z' - 'A' + 1];
		for(char c : J.toCharArray()){
			chars[c - 'A'] = 1;
		}

		int ret = 0;
		for(char c: S.toCharArray()){
			ret += chars[c - 'A'];
		}
		return ret;
	}

	public static void main(String[] args) {
		System.out.println((int)'z' + "\t" + (int)'A');
	}
}
