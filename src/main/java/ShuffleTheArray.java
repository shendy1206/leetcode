import org.junit.jupiter.api.DynamicTest;

import java.util.Arrays;

/**
 * Given the array nums consisting of 2n elements in the form [x1,x2,...,xn,y1,y2,...,yn].
 *
 * Return the array in the form [x1,y1,x2,y2,...,xn,yn].
 */
public class ShuffleTheArray {


	public int[] shuffle(int[] nums, int n) {
		if(nums == null || nums.length <= 2) return nums;

		int[] ret = new int[nums.length];


		for(int i = 0; i < n; i += 1){
			ret[i * 2] = nums[i];
			ret[i* 2 + 1] = nums[i + n];
		}

		return ret;

	}



}
