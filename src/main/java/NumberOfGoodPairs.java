import java.util.HashMap;
import java.util.Map;

/**
 * 1512. Number of Good Pairs
 * Easy
 *
 * 405
 *
 * 30
 *
 * Add to List
 *
 * Share
 * Given an array of integers nums.
 *
 * A pair (i,j) is called good if nums[i] == nums[j] and i < j.
 *
 * Return the number of good pairs.
 *
 *
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1,1,3]
 * Output: 4
 * Explanation: There are 4 good pairs (0,3), (0,4), (3,4), (2,5) 0-indexed.
 * Example 2:
 *
 * Input: nums = [1,1,1,1]
 * Output: 6
 * Explanation: Each pair in the array are good.
 * Example 3:
 *
 * Input: nums = [1,2,3]
 * Output: 0
 *
 *
 * Constraints:
 *
 * 1 <= nums.length <= 100
 * 1 <= nums[i] <= 100
 */
public class NumberOfGoodPairs {
	public int numIdenticalPairs(int[] nums) {

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		//1. find the size of number has same value
		for(int num : nums){
			Integer size = map.get(num);
			if(size == null){
				map.put(num, 1);
			}else{
				map.put(num, size + 1);
			}
		}

		//2. calculate pairs and sum up
		int pairs = 0;
		for(Map.Entry<Integer, Integer> e: map.entrySet()){
			pairs += e.getValue() * (e.getValue() - 1) / 2;
		}

		return pairs;


	}


}
