import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class ShuffleTheArrayTest {

	@Test
	public void test(){
		int[] ret = new ShuffleTheArray().shuffle(new int[]{2, 5, 1, 3, 4, 7}
				, 3);
		Assert.assertArrayEquals(new int[]{2, 3, 5, 4, 1, 7}, ret);
	}
	@Test
	public void test1(){

		int[] ret = new ShuffleTheArray().shuffle(new int[]{1,1,2,2}
				, 2);
		Assert.assertArrayEquals(new int[]{1,2,1,2}, ret);
	}
}
